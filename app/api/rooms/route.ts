import { NextResponse, NextRequest } from 'next/server';
import { cookies, headers } from 'next/headers';
import { rooms } from '@/fakeData/chat';
import { prisma } from '@/lib/prisma';

export async function GET(request: NextRequest) {
  const headersObj = headers();
  const userId = headersObj.get('Authorization');
  console.log('userId', userId);
  if (!userId) return NextResponse.json({ message: 'userId is not send' }, { status: 404, statusText: 'Not Found!' });

  const user = await prisma.user.findUnique({
    where: {
      id: userId,
    },
  });
  if (!user) return NextResponse.json({ message: 'user not found' }, { status: 404, statusText: 'Not Found!' });

  const rooms2 = await prisma.room.findMany({
    where: {
      authorId: user.id,
    },
  });
  console.log('rooms2', rooms2);

  return NextResponse.json(rooms);
}

export async function POST(request: NextRequest) {
  const userId = cookies().get('user_id')?.value;
  console.log('userId', userId);
  if (!userId) return NextResponse.json({ message: 'userId is not send' }, { status: 404, statusText: 'Not Found!' });

  const user = await prisma.user.findUnique({
    where: {
      id: userId,
    },
  });
  if (!user) return NextResponse.json({ message: 'user not found' }, { status: 404, statusText: 'Not Found!' });

  const body = (await request.json()) as { title: string };

  const room = await prisma.room.create({
    data: {
      title: body.title,
      authorId: user.id,
      users: {
        connect: {
          id: user.id,
        },
      },
    },
  });

  return NextResponse.json(room);
}
