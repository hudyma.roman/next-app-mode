import { NextResponse, NextRequest } from 'next/server';
import { chats } from '@/fakeData/chat';

interface Params {
  roomId: string;
}

export async function GET(request: NextRequest, { params }: { params: Params }) {
  const roomId = params.roomId;

  const chatsInRoom = chats.filter(chat => chat.relatedRoomId === roomId);

  return NextResponse.json(chatsInRoom);
}
