import { NextResponse, NextRequest } from 'next/server';
import { messages } from '@/fakeData/chat';

interface Params {
  chatId: string;
}

export async function GET(request: NextRequest, { params }: { params: Params }) {
  const chatId = params.chatId;

  const messagesInChat = messages.filter(message => message.relatedPerson === chatId);

  return NextResponse.json(messagesInChat);
}
