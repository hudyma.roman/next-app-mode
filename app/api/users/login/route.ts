import { prisma } from '@/lib/prisma';
import { Prisma, User } from '@prisma/client';
import { NextResponse, NextRequest } from 'next/server';
import { cookies } from 'next/headers';
interface Body {
  username?: string;
}

const findOrCreateUser = async ({ username }: Prisma.UserCreateInput): Promise<User> => {
  let user: User | null;
  user = await prisma.user.findUnique({
    where: {
      username,
    },
  });

  if (!user) {
    user = await prisma.user.create({
      data: {
        username,
      },
    });
  }
  return user;
};

export async function POST(request: NextRequest) {
  const body = (await request.json()) as Body;

  //TODO: Create error default response format
  if (!body?.username) return NextResponse.json({ message: 'username is not send' }, { status: 404, statusText: 'Not Found!' });

  const user = await findOrCreateUser({ username: body.username });
  cookies().set({
    name: 'user_id',
    value: user.id,
  });

  return NextResponse.json(user);
}
