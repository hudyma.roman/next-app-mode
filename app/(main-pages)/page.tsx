import type { NextPage } from 'next';
import LoginForm from './_components/LoginForm';

const Page: NextPage = async () => {
  return (
    <section className='flex flex-col items-center justify-center h-screen'>
      <LoginForm />
    </section>
  );
};

export default Page;
