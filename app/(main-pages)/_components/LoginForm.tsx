'use client';
import { User } from '@prisma/client';
import type { NextComponentType, NextPageContext } from 'next';
import { useRouter } from 'next/navigation';
import { useState } from 'react';

interface Props {}

const LoginForm: NextComponentType<NextPageContext, {}, Props> = (props: Props) => {
  const router = useRouter();
  const [username, setUsername] = useState('');

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      const res = await fetch(`${process.env.BASE_URL}/api/users/login`, {
        cache: 'no-store',
        method: 'POST',
        body: JSON.stringify({ username }),
      });
      const user = (await res.json()) as User;

      console.log('user', user);
      router.push('/rooms');
      setUsername('');
    } catch (error: any) {
      console.log(error);
    }
  };

  return (
    <form className='flex gap-x-5 font-bold text-xl' onSubmit={handleSubmit}>
      <label className='flex items-center justify-center px-8 py-4 rounded border-[4px] border-sky-200'>
        <input className='w-full h-full rounded outline-none' required minLength={3} type='text' value={username} onChange={e => setUsername(e.target.value.trim())} placeholder='Create username or Enter' />
      </label>
      <button className='flex items-center justify-center px-8 py-4 rounded bg-lime-400 text-white' type='submit'>
        Enter the Chat
      </button>
    </form>
  );
};

export default LoginForm;
