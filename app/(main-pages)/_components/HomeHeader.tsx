'use client';
import type { NextComponentType, NextPageContext } from 'next';
import { usePathname } from 'next/navigation';
import Link from 'next/link';

interface Props {}

const links = [
  { title: 'Home', href: '/' },
];

const HomeHeader: NextComponentType<NextPageContext, {}, Props> = (props: Props) => {
  const pathname = usePathname();
  const checkActiveLink = (href: string) => pathname === href;

  return (
    <header className='flex justify-between items-end px-5 fixed w-full'>
      <h1 className='text-4xl text-blue-600 tracking-wide'>Logo</h1>
      <nav className='flex gap-x-3 text-gray-400 underline'>
        {links.map(link => (
          <Link className={`hover:text-blue-600 ${checkActiveLink(link.href) && 'text-blue-400'}`} key={link.href} href={link.href}>
            {link.title}
          </Link>
        ))}
      </nav>
    </header>
  );
};

export default HomeHeader;
