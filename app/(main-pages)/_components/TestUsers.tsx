"use client"
import type { NextComponentType, NextPageContext } from 'next';
import { useGetUsersQuery } from '@/store/services/userApiTest';
interface Props {}

const TestUsers: NextComponentType<NextPageContext, {}, Props> = (props: Props) => {
  const { isLoading, isFetching, data, error } = useGetUsersQuery(null);
  return (
    <div>
      {error ? (
        <p>Oh no, there was an error</p>
      ) : isLoading || isFetching ? (
        <p>Loading...</p>
      ) : data ? (
        <div
          style={{
            display: 'grid',
            gridTemplateColumns: '1fr 1fr 1fr 1fr',
            gap: 20,
          }}
        >
          {data.map(user => (
            <div key={user.id} style={{ border: '1px solid #ccc', textAlign: 'center' }}>
              <h3>{user.name}</h3>
            </div>
          ))}
        </div>
      ) : null}
    </div>
  );
};

export default TestUsers;
