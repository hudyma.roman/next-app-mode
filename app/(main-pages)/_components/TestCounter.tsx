'use client';
import type { NextComponentType, NextPageContext } from 'next';
import { useSelector, useDispatch } from 'react-redux';
import { increment, decrement } from '@/store/counter';
import { selectCount } from '@/store/selectors';
interface Props {}

const TestCounter: NextComponentType<NextPageContext, {}, Props> = (props: Props) => {
  const dispatch = useDispatch();
  const count = useSelector(selectCount);
  return (
    <div className='flex gap-x-5 font-bold text-xl p-10 rounded border border-green-400 items-center'>
      <button className='p-2 border border-blue-400' onClick={() => dispatch(decrement())}>
        minus
      </button>
      <p>{count}</p>
      <button className='p-2 border border-blue-400 rounded' onClick={() => dispatch(increment())}>
        plus
      </button>
    </div>
  );
};

export default TestCounter;
