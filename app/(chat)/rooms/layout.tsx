'use client';

import RoomsList from '@/app/(chat)/_components/RoomsList';
import { useGetRoomsQuery } from '@/store/services/roomsApi';

export default function RoomsLayout({ children }: { children: React.ReactNode }) {
  const { data: rooms, error, isLoading } = useGetRoomsQuery(null);
  return (
    <section className='flex h-screen'>
      <RoomsList isLoading={isLoading} error={Boolean(error)} rooms={rooms} />
      {children}
    </section>
  );
}
