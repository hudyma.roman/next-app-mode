'use client';
import { useParams } from 'next/navigation';
import ChatsList from '@/app/(chat)/_components/ChatsList';
import { useGetPersonsQuery } from '@/store/services/roomsApi';

export default function RoomsTemplate({ children }: { children: React.ReactNode }) {
  const { room: roomId } = useParams();
  const { data: chats, error, isLoading } = useGetPersonsQuery({ roomId });

  return (
    <section className='flex h-screen w-full'>
      <ChatsList error={Boolean(error)} isLoading={isLoading} chats={chats} roomId={roomId} />
      {children}
    </section>
  );
}
