'use client';
import type { NextPage } from 'next';
import MessageInput from '@/app/(chat)/_components/MessageInput';
import MessagesList from '@/app/(chat)/_components/MessagesList';
import { useParams } from 'next/navigation';
import { useGetMessagesQuery } from '@/store/services/roomsApi';

interface Props {
  params: Params;
}

interface Params {}

const Page: NextPage<Props> = ({}) => {
  const { room: roomId, chat: chatId } = useParams();
  const { data: messages, error, isLoading } = useGetMessagesQuery({ roomId, chatId });
  return (
    <section className='w-full flex flex-col h-screen'>
      <MessagesList error={Boolean(error)} isLoading={isLoading} messages={messages} />
      <MessageInput />
    </section>
  );
};

export default Page;
