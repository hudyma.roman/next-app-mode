import type { NextPage } from 'next';

interface Props {}

const Page: NextPage<Props> = () => {
  return (
    <>
      <div className='w-full flex flex-col items-center justify-center'>
        <p className='text-4xl font-semibold tracking-wide text-blue-300'>Take the second step choose a chat</p>
      </div>
    </>
  );
};

export default Page;
