import type { NextPage } from 'next';

const Page: NextPage = () => {
  return (
    <div className='w-full flex flex-col items-center justify-center'>
      <p className='text-4xl font-semibold tracking-wide text-blue-300'>Take the first step choose a room</p>
    </div>
  );
};

export default Page;
