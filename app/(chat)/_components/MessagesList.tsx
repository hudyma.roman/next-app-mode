'use client';
import type { NextComponentType, NextPageContext } from 'next';
import MessageItem from './MessageItem';
import { IMessage } from '@/fakeData/chat';
import { useEffect } from 'react';
interface Props {
  messages?: IMessage[];
  error?: boolean;
  isLoading?: boolean;
}

const MessagesList: NextComponentType<NextPageContext, {}, Props> = ({ messages, error, isLoading }: Props) => {
  useEffect(() => {
    const messagesListDom = document.getElementById('message-list');
    if (!messagesListDom) return;
    messagesListDom.scrollTop = messagesListDom.scrollHeight;
  }, [messages]);
  return (
    <ul id='message-list' className='flex flex-col gap-y-5 p-5 items-start max-h-full overflow-y-auto overflow-x-auto'>
      {isLoading && <p>Loading ...</p>}
      {error && <p>Oops! We caught an error!</p>}
      {messages && messages.map(message => <MessageItem key={message.id} message={message} />)}
    </ul>
  );
};

export default MessagesList;
