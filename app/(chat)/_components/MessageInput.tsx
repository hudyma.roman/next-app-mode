import type { NextComponentType, NextPageContext } from 'next';

interface Props {}

const MessageInput: NextComponentType<NextPageContext, {}, Props> = (props: Props) => {
  return (
    <div className='h-[120px] min-h-[120px] p-5 mt-auto'>
      <div className='bg-sky-300 flex gap-x-5 w-full h-full rounded-3xl px-4'>
        <textarea className='bg-transparent text-white font-semibold w-full rounded-3xl py-2 no-scrollbar outline-none resize-none' />
        <button className='text-white font-semibold'>Send</button>
      </div>
    </div>
  );
};

export default MessageInput;
