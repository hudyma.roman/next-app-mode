'use client';
import type { NextComponentType, NextPageContext } from 'next';
import { useRef } from 'react';

interface Props {
  open?: boolean;
  children: React.ReactNode;
  closeEvent?: () => void;
}

const Modal: NextComponentType<NextPageContext, {}, Props> = ({ open, closeEvent, children }: Props) => {
  const elementRef = useRef<HTMLDivElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
    if (event.target === elementRef.current) {
      console.log('Clicked on self');
      closeEvent && closeEvent();
    }
  };
  return (
    <>
      {open && (
        <div ref={elementRef} onClick={handleClick} className='fixed bg-slate-700 bg-opacity-50 top-0 left-0 w-full h-screen flex flex-col items-center justify-center z-[100]'>
          {children}
        </div>
      )}
    </>
  );
};

export default Modal;
