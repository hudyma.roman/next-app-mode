import { Room } from '@prisma/client';
import type { NextComponentType, NextPageContext } from 'next';
import { useState } from 'react';

interface Props {
  successEvent?: () => void;
}

const CreateRoomForm: NextComponentType<NextPageContext, {}, Props> = ({ successEvent }: Props) => {
  const [roomTitle, setRoomTitle] = useState('');
  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      const res = await fetch(`${process.env.BASE_URL}/api/rooms`, {
        cache: 'no-store',
        method: 'POST',
        body: JSON.stringify({ title: roomTitle }),
      });
      const room = (await res.json()) as Room;

      console.log('room', room);
      successEvent && successEvent();
    } catch (error: any) {
      console.log(error);
    }
  };
  return (
    <div className='bg-white py-5 px-7 rounded'>
      <form className='flex gap-x-5 font-bold text-xl' onSubmit={handleSubmit}>
        <label className='flex items-center justify-center px-8 py-4 rounded border-[4px] border-sky-200'>
          <input className='w-full h-full rounded outline-none' required minLength={3} type='text' value={roomTitle} onChange={e => setRoomTitle(e.target.value)} placeholder='Room title' />
        </label>
        <button className='flex items-center justify-center px-8 py-4 rounded bg-lime-400 text-white' type='submit'>
          Create room
        </button>
      </form>
    </div>
  );
};

export default CreateRoomForm;
