'use client';
import { IMessage } from '@/fakeData/chat';
import type { NextComponentType, NextPageContext } from 'next';

interface Props {
  message: IMessage;
}
const checkIfAuthorIsMe = (message: IMessage) => message.author === 'self';

const MessageItem: NextComponentType<NextPageContext, {}, Props> = ({ message }: Props) => {
  return (
    <li className={`flex flex-col gap-y-2 py-1 px-4 rounded max-w-[90%] ${checkIfAuthorIsMe(message) ? 'bg-green-100 self-end text-right' : 'bg-blue-100'}`}>
      <p className={"font-semibold"}>{checkIfAuthorIsMe(message) ? 'You' : message.author}</p>
      <p>{message.message}</p>
    </li>
  );
};

export default MessageItem;
