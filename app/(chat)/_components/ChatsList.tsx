import { IPerson } from '@/fakeData/chat';
import type { NextComponentType, NextPageContext } from 'next';
import ListItem from './ListItem';
import { useParams } from 'next/navigation';

interface Props {
  chats?: IPerson[];
  roomId: string;
  error?: boolean;
  isLoading?: boolean;
}

const ChatsList: NextComponentType<NextPageContext, {}, Props> = ({ chats, roomId, error, isLoading }: Props) => {
  const routerParams = useParams();
  const chatId = routerParams.chat;

  return (
    <div className='flex flex-col justify-between py-5 px-7 bg-sky-400 text-white'>
      <ul className='flex flex-col gap-y-3 w-[180px]'>
        {isLoading && <p>Loading ...</p>}
        {error && <p>Oops! We caught an error!</p>}
        {chats &&
          chats.map(chat => (
            <li key={chat.id}>
              <ListItem isActive={chatId === chat.id} href={`/rooms/${roomId}/${chat.id}`}>
                {chat.name}
              </ListItem>
            </li>
          ))}
      </ul>
    </div>
  );
};

export default ChatsList;
