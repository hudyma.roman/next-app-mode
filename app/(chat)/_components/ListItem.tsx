import type { NextComponentType, NextPageContext } from 'next';
import Link from 'next/link';

interface Props {
  isActive?: boolean;
  href: string;
  children: React.ReactNode;
}

const ListItem: NextComponentType<NextPageContext, {}, Props> = ({ href = '/', isActive, children }: Props) => {
  return (
    <div className={isActive ? 'text-red-500' : ''}>
      <Link className='hover:underline' href={href}>
        {children}
      </Link>
    </div>
  );
};

export default ListItem;
