'use client';
import { IRoom } from '@/fakeData/chat';
import type { NextComponentType, NextPageContext } from 'next';
import Link from 'next/link';
import ListItem from './ListItem';
import { useParams } from 'next/navigation';
import Modal from './Modal';
import { useState } from 'react';
import CreateRoomForm from './CreateRoomForm';

interface Props {
  rooms?: IRoom[];
  error?: boolean;
  isLoading?: boolean
}

const RoomsList: NextComponentType<NextPageContext, {}, Props> = ({ rooms = [], error, isLoading }: Props) => {
  console.log('rooms', rooms);
  const routerParams = useParams();
  const roomId = routerParams.room;

  const [createModalOpen, setCreateModalOpen] = useState(false);

  const openCreateModal = () => {
    setCreateModalOpen(true);
  };

  return (
    <>
      <Modal closeEvent={() => setCreateModalOpen(false)} open={createModalOpen}>
        <CreateRoomForm successEvent={() => setCreateModalOpen(false)} />
      </Modal>

      <div className='flex flex-col py-5 px-7 gap-y-5 bg-purple-900 text-white'>
        {isLoading && <p>Loading ...</p>}
        {error && <p>Oops! We caught an error!</p>}
        <ul className='flex flex-col gap-y-3 capitalize w-[180px] mb-auto'>
          {rooms && rooms.map(room => (
            <li key={room.id}>
              <ListItem isActive={roomId === room.id} href={`/rooms/${room.id}`}>
                {room.title}
              </ListItem>
            </li>
          ))}
        </ul>
        <button onClick={openCreateModal} className='flex'>
          Create room
        </button>
        <Link href='/'>Back to Home</Link>
      </div>
    </>
  );
};

export default RoomsList;
