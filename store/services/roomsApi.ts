import { IMessage, IPerson, IRoom } from '@/fakeData/chat';
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { getCookie } from 'cookies-next';

const user_id = getCookie('user_id');

export const roomsApi = createApi({
  reducerPath: 'roomsApi',
  refetchOnFocus: true,
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.BASE_URL}/api`,
    headers: {
      Authorization: `${user_id || ''}`,
    },
  }),
  endpoints: builder => ({
    getRooms: builder.query<IRoom[], null>({
      query: () => '/rooms',
    }),
    getPersons: builder.query<IPerson[], { roomId: string }>({
      query: ({ roomId }) => `/rooms/${roomId}`,
    }),
    getMessages: builder.query<IMessage[], { roomId: string; chatId: string }>({
      query: ({ roomId, chatId }) => `/rooms/${roomId}/${chatId}`,
    }),
  }),
});

export const { useGetRoomsQuery, useGetPersonsQuery, useGetMessagesQuery } = roomsApi;
