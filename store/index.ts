import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '@/store/counter';
import { userApiTest } from './services/userApiTest';
import { roomsApi } from './services/roomsApi';
import { setupListeners } from '@reduxjs/toolkit/dist/query';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    [userApiTest.reducerPath]: userApiTest.reducer,
    [roomsApi.reducerPath]: roomsApi.reducer,
  },
  devTools: process.env.NODE_ENV !== 'production',
  middleware: getDefaultMiddleware => getDefaultMiddleware({}).concat([userApiTest.middleware]).concat([roomsApi.middleware]),
});

setupListeners(store.dispatch);

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
