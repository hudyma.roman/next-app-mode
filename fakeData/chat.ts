import * as chatDataRaw from './chatData.json';
const chatDataObj = chatDataRaw;

export interface IRoom {
  title: string;
  id: string;
}

export interface IMessage {
  id: string;
  author: string;
  message: string;
  image: string;
  relatedPerson: string;
}

const roomsData = chatDataObj.chatData.map(room => ({ id: room.id, title: room.title }));

const chatsData: IPerson[] = [];

const messagesData: IMessage[] = [];

chatDataObj.chatData.forEach(room => {
  room.persons.forEach(person => {
    chatsData.push({ id: person.id, name: person.name, relatedRoomId: person.relatedRoomId });

    person.messages.forEach(message => {
      messagesData.push({ id: message.id, author: message.author, image: message.image, message: message.message, relatedPerson: message.relatedPerson });
    });
  });
});

export const rooms: IRoom[] = roomsData;

export interface IPerson {
  relatedRoomId: string;
  name: string;
  id: string;
}

export const chats = chatsData;
export const messages = messagesData;
